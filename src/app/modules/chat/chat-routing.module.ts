import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatLayoutComponent } from './chat-layout/chat-layout.component';
// import { AuthGuard } from 'src/app/guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        // canActivate: [AuthGuard],
        component: ChatLayoutComponent
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ChatRoutingModule { }