import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatRoutingModule } from './chat-routing.module';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ChatLayoutComponent } from './chat-layout/chat-layout.component';
import { UserListComponent } from './chat-layout/user-list/user-list.component';
import { ConversationComponent } from './chat-layout/conversation/conversation.component';
import { MessageComponent } from './chat-layout/message/message.component';

@NgModule({
    declarations: [ChatLayoutComponent, UserListComponent, ConversationComponent, MessageComponent],
    imports: [
        CommonModule,
        ChatRoutingModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        MatCardModule
    ]
})

export class ChatModule { }
