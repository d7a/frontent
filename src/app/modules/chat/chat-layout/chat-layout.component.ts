import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services/message.service';
import { ChatUserService } from 'src/app/services/chat-user.service';

@Component({
    selector: 'app-chat-layout',
    templateUrl: './chat-layout.component.html',
    styleUrls: ['./chat-layout.component.css']
})

export class ChatLayoutComponent implements OnInit {

    userList: any;
    messageList: any;
    messageForm: any;

    currentUser: string;

    constructor(
        private messageService: MessageService,
        private chatUserService: ChatUserService
    ) {

        this.currentUser = 'Random name';

        this.chatUserService.create({name: this.currentUser}).then((res) => {
            console.log(res);
        });

        this.chatUserService.userList.subscribe((users) => {
            this.userList = users;
        });

        this.messageService.messageList.subscribe((messages) => {
            this.messageList = messages;
        });

        this.messageForm = this.messageService.getMessageForm();
    }

    ngOnInit() {
    }

    sendMessage() {

        this.messageForm.get('sender').setValue(this.currentUser);
        const messageData = this.messageForm.value;
        console.log(messageData);
        this.messageService.create(messageData).then((res) => {
            console.log(res);
        });
    }

}
