import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatModule } from './modules/chat/chat.module';
import { ErrorModule } from './modules/error/error.module';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => ChatModule,
        // canActivate: [AuthGuard]
    },
    {
        path: '**',
        loadChildren: () => ErrorModule
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
