import { Injectable } from '@angular/core';
import { RestService } from './api/rest.service';

@Injectable({
    providedIn: 'root'
})

export class ConversationService {

    private feathersConversationService;

    constructor(
        private restService: RestService
    ) {
        const api = this.restService.getFeathersAPI();
        this.feathersConversationService = api.service('conversation');
    }

    public create( data ) {
        return this.feathersConversationService.create( data );
    }

    public get( contentId = null, param = {} ) {
        return this.feathersConversationService.get( contentId, param );
    }

    public find( param ) {
        return this.feathersConversationService.find( param );
    }

    public update( id, data, params = {}) {
        return this.feathersConversationService.update( id, data, params );
    }

    public patch( id = null, data = {}, params = {} ) {
        return this.feathersConversationService.patch( id, data, params );
    }

    public remove( id, params = {} ) {
        return this.feathersConversationService.remove( id, params ).then(( result ) => {
            return result;
        }).catch((error) => {
            throw error;
        });
    }
}
