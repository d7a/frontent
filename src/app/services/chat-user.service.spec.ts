import { TestBed } from '@angular/core/testing';

import { ChatUserService } from './chat-user.service';

describe('ChatUserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChatUserService = TestBed.get(ChatUserService);
    expect(service).toBeTruthy();
  });
});
