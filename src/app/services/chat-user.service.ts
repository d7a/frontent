import { Injectable } from '@angular/core';
import { RestService } from './api/rest.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class ChatUserService {

    private feathersChatUserService;
    userList = new BehaviorSubject<any>(null);

    constructor(
        private restService: RestService
    ) {
        const api = this.restService.getFeathersAPI();
        this.feathersChatUserService = api.service('chat-user');

        this.find({query: {}}).then((res) => {
            this.userList.next(res.data);
        });

        this.feathersChatUserService.on('created', (result) => {
            this.find({query: {}}).then((res) => {
                this.userList.next(res.data);
            });
        });
    }

    public create( data ) {
        return this.feathersChatUserService.create( data );
    }

    public get( contentId = null, param = {} ) {
        return this.feathersChatUserService.get( contentId, param );
    }

    public find( param ) {
        return this.feathersChatUserService.find( param );
    }

    public update( id, data, params = {}) {
        return this.feathersChatUserService.update( id, data, params );
    }

    public patch( id = null, data = {}, params = {} ) {
        return this.feathersChatUserService.patch( id, data, params );
    }

    public remove( id, params = {} ) {
        return this.feathersChatUserService.remove( id, params ).then(( result ) => {
            return result;
        }).catch((error) => {
            throw error;
        });
    }
}
