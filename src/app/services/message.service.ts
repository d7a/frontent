import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RestService } from 'src/app/services/api/rest.service';
import { AuthService } from 'src/app/services/api/auth.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class MessageService {

    private feathersChatMessageService;
    messageList = new BehaviorSubject<any>(null);

    constructor(
        private restService: RestService,
        private authService: AuthService,
        private formBuilder: FormBuilder
    ) {
        const api = this.restService.getFeathersAPI();
        this.feathersChatMessageService = api.service('message');

        this.find({query: {}}).then((res) => {
            this.messageList.next(res.data);
        });

        this.feathersChatMessageService.on('created', (result) => {
            this.find({query: {}}).then((res) => {
                this.messageList.next(res.data);
            });
        });
    }

    public create( data ) {
        return this.feathersChatMessageService.create( data );
    }

    public get( contentId = null, param = {} ) {
        return this.feathersChatMessageService.get( contentId, param );
    }

    public find( param ) {
        return this.feathersChatMessageService.find( param );
    }

    public update( id, data, params = {}) {
        return this.feathersChatMessageService.update( id, data, params );
    }

    public patch( id = null, data = {}, params = {} ) {
        return this.feathersChatMessageService.patch( id, data, params );
    }

    public remove( id, params = {} ) {
        return this.feathersChatMessageService.remove( id, params ).then(( result ) => {
            return result;
        }).catch((error) => {
            throw error;
        });
    }

    public getMessageForm() {
        return this.formBuilder.group( {
            _id: [],
            // conversationId: ['', Validators.required],
            sender: ['', Validators.required],
            text: ['', Validators.required],
            sendTime: []
        });
    }
}
