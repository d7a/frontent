import { Injectable } from '@angular/core';
import { restConfig } from 'src/config/rest.config';

import * as io from 'socket.io-client';
import * as feathers from '@feathersjs/client';
import socketio from '@feathersjs/socketio-client';
import auth from '@feathersjs/authentication-client';

@Injectable({
    providedIn: 'root',
})

export class RestService {

    private api;

    constructor() {

        this.api = new feathers();

        const url = restConfig.host + ':' + restConfig.port;

        const socket = io(url, {
            transports: ['websocket'],
            forceNew: true
        });

        this.api.configure(socketio(socket));

        const authOptions = {
            header: 'Authorization', // the default authorization header for REST
            path: '/authentication', // the server-side authentication service path
            jwtStrategy: 'jwt', // the name of the JWT authentication strategy
            entity: 'user', // the entity you are authenticating (ie. a users)
            service: 'users', // the service to look up the entity
            cookie: 'feathers-jwt', // the name of the cookie to parse the JWT from when cookies are enabled server side
            storageKey: 'feathers-jwt', // the key to store the accessToken in localstorage or AsyncStorage on React Native
            storage: window.localStorage // Passing a WebStorage-compatible object to enable automatic storage on the client.
        };

        this.api.configure( auth(authOptions) );
    }

    public getFeathersAPI() {
        return this.api;
    }
}
