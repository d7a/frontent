import { Injectable } from '@angular/core';
import { restConfig } from 'src/config/rest.config';
import { RestService } from 'src/app/services/api/rest.service';

@Injectable({
    providedIn: 'root',
})

export class AuthService {

    private api;

    constructor(private restService: RestService) {
        this.api = this.restService.getFeathersAPI();
    }

    public isAuthorized() {
        return this.api.authenticate().then((response) => {
            return response.accessToken;
        }).catch((e) => {
            return false;
        });
    }

    public login(email: string, password: string) {
        return this.api.authenticate({
			strategy: 'local',
            email: email,
            password: password
		}).then((data) => {
    		return data;
		}).catch(e => {
		    return e;
		});
    }

	public logout() {
        this.api.logout();
	}

	public getAuthorizedUserId() {
        return this.isAuthorized().then(( accessToken ) => {
            return this.api.passport.verifyJWT( accessToken ).then((response) => {
                return response.userId;
            });
        });
    }
}
